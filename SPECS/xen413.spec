# build xsm support unless rpmbuild was run with --without xsm
# or required packages are missing
%define with_xsm  %{?_without_xsm: 0} %{?!_without_xsm: 1}
%define build_xsm %(test -x %{_bindir}/checkpolicy && test -x %{_bindir}/m4 && echo %{with_xsm} || echo 0)
# cross compile 64-bit hypervisor on ix86 unless rpmbuild was run
# with --without crosshyp

# Hypervisor ABI
%define hv_abi 4.13
%define build_hyp 1

## Configure for target system
%if 0%{rhel} == 7
%define with_systemd_presets 1
%define with_systemd 1
%define build_ocaml 1
%define with_ocaml 1
%endif
%if 0%{rhel} >= 8
%define with_systemd_presets 1
%define with_systemd 1
%define build_ocaml 0
%define with_ocaml 0
%endif

%global _source_payload w5.xzdio
%global _binary_payload w5.xzdio
%global _default_patch_fuzz 2
%{!?ocamllib: %global ocamllib %(ocamlfind printconf stdlib)}

Summary: Xen is a virtual machine monitor
Name:    xen413
Version: 4.13.2
Release: 7%{?dist}
Group:   Development/Libraries
License: GPLv2+ and LGPLv2+ and BSD
URL:     http://xen.org/

%define debug_package %{nil}
Source0: https://downloads.xenproject.org/release/xen/%{version}/xen-%{version}.tar.gz
Source2: xen.logrotate
Source3: https://www.seabios.org/downloads/seabios-1.10.2.tar.gz
Source4: http://xenbits.xen.org/xen-extfiles/ipxe-git-356f6c1b64d7a97746d1816cef8ca22bdd8d0b5d.tar.gz

# used by stubdoms
Source10: http://xenbits.xen.org/xen-extfiles/lwip-1.3.0.tar.gz
Source11: http://xenbits.xen.org/xen-extfiles/newlib-1.16.0.tar.gz
Source12: http://xenbits.xen.org/xen-extfiles/zlib-1.2.3.tar.gz
Source13: http://xenbits.xen.org/xen-extfiles/pciutils-2.2.9.tar.bz2
Source14: http://xenbits.xen.org/xen-extfiles/grub-0.97.tar.gz
Source15: http://xenbits.xen.org/xen-extfiles/polarssl-1.1.4-gpl.tgz
Source16: http://xenbits.xen.org/xen-extfiles/tpm_emulator-0.7.4.tar.gz
Source17: http://xenbits.xen.org/xen-extfiles/gmp-4.3.2.tar.bz2

## SysV init scripts
Source20: init.xendomains

# sysconfig bits
Source25: sysconfig.xenstored
Source26: sysconfig.xenconsoled

## Systemd Unit files
Source30: xendomains.service
Source31: proc-xen.mount
Source32: var-lib-xenstored.mount
Source33: xenstored.service
Source34: xenconsoled.service
Source35: xen-watchdog.service
Source36: tmpfiles.d.xen.conf

Patch1: 0001-brctl-cleanup.patch
Patch2: 0002-brctl-and-ip.patch
Patch100: xen-patches.am

## List all build requirements
BuildRequires: transfig libidn-devel zlib-devel texi2html
BuildRequires: ghostscript texlive-latex
BuildRequires: SDL-devel curl-devel gtk2-devel
BuildRequires: git
BuildRequires: ncurses-devel libaio-devel
# for the docs
BuildRequires: perl texinfo graphviz
# so that the makefile knows to install udev rules
BuildRequires: udev
BuildRequires: gettext
BuildRequires: openssl-devel
# For ioemu PCI passthrough
BuildRequires: pciutils-devel
# Several tools now use uuid
BuildRequires: libuuid-devel
# iasl needed to build hvmloader
BuildRequires: iasl
# modern compressed kernels
BuildRequires: bzip2-devel xz-devel
# libfsimage
BuildRequires: e2fsprogs-devel
# tools now require yajl and wget
BuildRequires: yajl-devel

%if %with_systemd
BuildRequires: systemd-devel
%endif

## Specific RHEL8 requirements.
%if 0%{rhel} >= 8
%undefine __brp_mangle_shebangs
%global __python %{__python3}
BuildRequires: python36 python36-devel hostname
%endif

## RHEL7 or earlier requirements
%if 0%{rhel} == 7
BuildRequires: python python-devel hostname
%endif

%ifarch x86_64
# so that x86_64 builds pick up glibc32 correctly
BuildRequires: /usr/include/gnu/stubs-32.h
%endif

%if %{with_ocaml}
# Can only build ocaml tools on el7 now due to upstream changes.
BuildRequires: ocaml ocaml-findlib
%endif

# build using Fedora seabios and ipxe packages for roms
BuildRequires: seabios-bin ipxe-roms-qemu
%if %with_xsm
# xsm policy file needs needs checkpolicy and m4
BuildRequires: checkpolicy m4
%endif

%if 0%{rhel} >= 8
Requires: python36 python3-lxml
%else
Requires: bridge-utils
Requires: python python-lxml
%endif
Requires: perl
Requires: udev >= 059
Requires: xen413-hypervisor = %{version}-%{release}
Requires: xen413-runtime = %{version}-%{release}
%if %{with_ocaml}
Requires: xen413-ocaml = %{version}-%{release}
%else
Obsoletes: xen413-ocaml
%endif
Requires: xen413-licenses = %{version}-%{release}
Requires: xen413-libs = %{version}-%{release}

## Conflict with any older version of Xen to prevent pain.
Conflicts: xen xen44 xen45 xen46 xen47 xen49 xen410
ExclusiveArch: %{ix86} x86_64

%if %with_systemd_presets
Requires(post): systemd
Requires(preun): systemd
Requires(postun): systemd
BuildRequires: systemd
%endif

%description
This package contains common Xen tools used to manage virtual machines
running under the Xen hypervisor

%package libs
Summary: Libraries for Xen tools
Group: Development/Libraries
Requires(pre): /sbin/ldconfig
Requires(post): /sbin/ldconfig
Requires: xen413 = %{version}-%{release}

%description libs
This package contains the libraries needed to run applications
which manage Xen virtual machines.


%package runtime
Summary: Core Xen runtime environment
Group: Development/Libraries
Requires: /usr/bin/qemu-img
Requires: xen-hypervisor-abi = %{hv_abi}
Requires: xen413 = %{version}-%{release}
Requires: xen413-libs = %{version}-%{release}

%description runtime
This package contains the runtime programs and daemons which
form the core Xen userspace environment.


%package hypervisor
Summary: Libraries for Xen tools
Group: Development/Libraries
Provides: xen-hypervisor-abi = %{hv_abi}
Provides: xen413-hypervisor-abi = %{hv_abi}
Requires: xen413 = %{version}-%{release}

%description hypervisor
This package contains the Xen hypervisor


%package doc
Summary: Xen documentation
Group: Documentation
#BuildArch: noarch
Requires: xen413 = %{version}-%{release}

%description doc
This package contains the Xen documentation.


%package devel
Summary: Development libraries for Xen tools
Group: Development/Libraries
Requires: xen413-libs = %{version}-%{release}
Requires: libuuid-devel
Requires: xen413 = %{version}-%{release}

%description devel
This package contains what's needed to develop applications
which manage Xen virtual machines.


%package licenses
Summary: License files from Xen source
Group: Documentation
Requires: xen413 = %{version}-%{release}

%description licenses
This package contains the license files from the source used
to build the xen packages.

%if %{with_ocaml}
%package ocaml
Summary: Ocaml libraries for Xen tools
Group: Development/Libraries
Requires: ocaml-runtime, xen413-libs = %{version}-%{release}
Requires: xen413 = %{version}-%{release}

%description ocaml
This package contains libraries for ocaml tools to manage Xen
virtual machines.

%package ocaml-devel
Summary: Ocaml development libraries for Xen tools
Group: Development/Libraries
Requires: xen413-ocaml = %{version}-%{release}
Requires: xen413 = %{version}-%{release}

%description ocaml-devel
This package contains libraries for developing ocaml tools to
manage Xen virtual machines.
%endif

%prep
%setup -q -n xen-%{version}

## If we have a xen-patches.patch greater than 0 bytes, apply it.
if [ -s %{PATCH100} ]; then
	# Create a git repo within the expanded tarball.
	git init
	git config user.email "netwiz@crc.id.au"
	git config user.name "Xen Made Easy"
	git config gc.auto 0
	# Have to remove the .gitignore so that tools/hotplug/Linux/init.d actually get included in the git tree
	rm -f .gitignore
	git add .
	git commit -a -q -m "%{version} baseline."

	# Apply patches to code in the core Xen repo
	git am %{PATCH100}
fi

%patch1 -p1
%patch2 -p1

# Prevent the build system from using information of this temporary git tree
rm -rf .git

cp -v %{SOURCE3} tools/firmware
(cd ./tools/firmware;tar xzf %{SOURCE3}; mv `tar -tzf %{SOURCE3} | sed -e 's@/.*@@' | uniq` seabios-dir)
cp -v %{SOURCE4} tools/firmware/etherboot
(cd ./tools/firmware/etherboot;tar xzf %{SOURCE4})

# stubdom sources
cp -v %{SOURCE10} %{SOURCE11} %{SOURCE12} %{SOURCE13} %{SOURCE14} %{SOURCE15} %{SOURCE16} %{SOURCE17} stubdom

############ build ################
%build
%if %{with_ocaml}
%define enable_ocaml "--enable-ocamltools"
mkdir -p dist/install%{ocamllib}/stublibs
%endif

%if 0%{rhel} >= 8
## Currently, scripts that can be python3 compatible still use #!/usr/bin/env python
## This means rpmbuild automatically adds a depend on /usr/bin/python - which is bs,
## This hack replaces all of those with the path to python3 - which may wholesale fix and wholesale break scripts.
sed -i '1s=^#!/usr/bin/\(python\|env python\)[0-9.]*=#!%{__python3}=' `grep -rE '^#!/usr/bin/(python|env python)' . | cut -d: -f1`
%endif

%if %with_systemd
./configure --enable-systemd --prefix=/usr --enable-xsmpolicy %{?enable_ocaml} \
  --libdir=%{_libdir} --disable-qemu-traditional \
  --with-extra-qemuU-configure-args="--enable-spice --enable-usb-redir"
%else
./configure --prefix=/usr --enable-xsmpolicy \
  --libdir=%{_libdir} --disable-qemu-traditional \
  --with-extra-qemuU-configure-args="--enable-spice --enable-usb-redir"
%endif

## Enable live patching, pvh, and make sure a few other things are on
(cd xen; \
 make defconfig; \
 sed -i 's/# CONFIG_LIVEPATCH is not set/CONFIG_LIVEPATCH=y/g' .config; \
 sed -i 's/# CONFIG_XEN_PVH is not set/CONFIG_XEN_PVH=y/g' .config; \
 sed -i 's/# CONFIG_XEN_PVHVM is not set/CONFIG_XEN_PVHVM=y/g' .config; \
 sed -i 's/# CONFIG_PARAVIRT is not set/CONFIG_PARAVIRT=y/g' .config; \
 sed -i 's/# CONFIG_PARAVIRT_GUEST is not set/CONFIG_PARAVIRT_GUEST=y/g' .config; \
 sed -i 's/# CONFIG_PARAVIRT_SPINLOCKS is not set/CONFIG_PARAVIRT_SPINLOCKS=y/g' .config; \
 sed -i 's/CONFIG_PV_LINEAR_PT.*/# CONFIG_PV_LINEAR_PT is not set/' .config; \
 cp .config ../../413.config; \
 make oldconfig
)

make %{?_smp_mflags} dist

%install
rm -rf %{buildroot}
%if %{with_ocaml}
mkdir -p %{buildroot}/%{ocamllib}/stublibs
%endif

mkdir -p %{buildroot}/usr/share/doc/xen/html/
%{?ocaml_flags} dist/install.sh %{buildroot}

%if %build_xsm
# policy file should be in /boot/flask
mkdir %{buildroot}/boot/flask
mv %{buildroot}/boot/xenpolicy* %{buildroot}/boot/flask
%else
rm -f %{buildroot}/boot/xenpolicy*
%endif

############ troubleshoot packaging: list files ############
find %{buildroot} -print | xargs ls -ld | sed -e 's|.*%{buildroot}||' > f1.list

############ kill unwanted stuff ############
# unwanted debug files
rm -rf %{buildroot}/usr/lib/debug
rm -rf %{buildroot}/usr/lib/.build-id

# stubdom: newlib
rm -rf %{buildroot}/usr/*-xen-elf

# hypervisor symlinks
rm -rf %{buildroot}/boot/xen-4.0.gz
rm -rf %{buildroot}/boot/xen-4.gz
%if !%build_hyp
rm -rf %{buildroot}/boot
%endif

# silly doc dir fun
rm -rf %{buildroot}%{_datadir}/doc/qemu

# Pointless helper
rm -f %{buildroot}%{_sbindir}/xen-python-path
rm -rf %{buildroot}%{_libdir}exec/
rm -rf %{buildroot}%{_libdir}/xen/libexec/

# qemu stuff (unused or available from upstream)
rm -rf %{buildroot}/usr/share/xen/man

# README's not intended for end users
rm -f %{buildroot}/%{_sysconfdir}/xen/README*

# standard gnu info files
rm -rf %{buildroot}/usr/info

# adhere to Static Library Packaging Guidelines
rm -rf %{buildroot}/%{_libdir}/*.a

# Remove icons and desktop files from qemu
rm -rf %{buildroot}/usr/share/qemu-xen/applications/*.desktop
rm -rf %{buildroot}/usr/share/qemu-xen/icons/hicolor/

# clean up extra efi files
rm -rf %{buildroot}/usr/lib64/efi

############ fixup files in /etc ############
# modules
mkdir -p %{buildroot}%{_sysconfdir}/sysconfig/modules

# logrotate
mkdir -p %{buildroot}%{_sysconfdir}/logrotate.d/
install -m 644 %{SOURCE2} %{buildroot}%{_sysconfdir}/logrotate.d/xen

# sysconfig
mkdir -p %{buildroot}%{_sysconfdir}/sysconfig
install -m 644 %{SOURCE25} %{buildroot}%{_sysconfdir}/sysconfig/xenstored
install -m 644 %{SOURCE26} %{buildroot}%{_sysconfdir}/sysconfig/xenconsoled

# Copy systemd or initscript service files.
%if %with_systemd
mkdir -p %{buildroot}%{_libexecdir}
install -m 755 %{SOURCE20} %{buildroot}%{_libexecdir}/xendomains

mkdir -p %{buildroot}%{_unitdir}
install -m 644 %{SOURCE30} %{buildroot}%{_unitdir}/xendomains.service
install -m 644 %{SOURCE31} %{buildroot}%{_unitdir}/proc-xen.mount
install -m 644 %{SOURCE32} %{buildroot}%{_unitdir}/var-lib-xenstored.mount
install -m 644 %{SOURCE33} %{buildroot}%{_unitdir}/xenstored.service
install -m 644 %{SOURCE34} %{buildroot}%{_unitdir}/xenconsoled.service
install -m 644 %{SOURCE35} %{buildroot}%{_unitdir}/xen-watchdog.service

mkdir -p %{buildroot}/usr/lib/tmpfiles.d
install -m 644 %{SOURCE36} %{buildroot}/usr/lib/tmpfiles.d/xen.conf
%else
install -m 755 %{SOURCE20} %{buildroot}%{_sysconfdir}/rc.d/init.d/xendomains
%endif

############ create dirs in /var ############
mkdir -p %{buildroot}%{_localstatedir}/lib/xen/images
mkdir -p %{buildroot}%{_localstatedir}/log/xen/console

############ troubleshoot packaging: list files ############
find %{buildroot} -print | xargs ls -ld | sed -e 's|.*%{buildroot}||' > f2.list
diff -u f1.list f2.list || true

# new in 4.5
(cd %{buildroot}; find . -type f -or -type l | sed -e s/^.// -e /^$/d) | sort > main.lst

grep '^%{_sbindir}' main.lst | sed -e '/oxenstored/ d' | sort > runtime.lst

############ assemble license files ############
mkdir licensedir
# avoid licensedir to avoid recursion, also stubdom/ioemu and dist
# which are copies of files elsewhere
find . -path licensedir -prune -o -path stubdom/ioemu -prune -o \
  -path dist -prune -o -name COPYING -o -name LICENSE | while read file; do
  mkdir -p licensedir/`dirname $file`
  install -m 644 $file licensedir/$file
done

############ all done now ############

%post
%if %with_systemd
/usr/bin/systemctl enable xendomains.service
%else
/sbin/chkconfig --add xendomains
/sbin/chkconfig --add xendriverdomain
%endif

%preun
%if %with_systemd
if [ $1 = 0 ]; then
	/usr/bin/systemctl disable xendomains.service
fi
%else
if [ $1 = 0 ]; then
	/sbin/chkconfig --del xendomains
fi
%endif

%post runtime
%if %with_systemd
%systemd_post xenstored.service xenconsoled.service xen-qemu-dom0-disk-backend.service
/usr/bin/systemctl enable xenstored.service
/usr/bin/systemctl enable xen-qemu-dom0-disk-backend.service
/usr/bin/systemctl enable xenconsoled.service
%else
/sbin/chkconfig --add xencommons
/sbin/chkconfig --add xen-watchdog
%endif


%preun runtime
%if %with_systemd
if [ $1 = 0 ]; then
	%systemd_preun xenstored.service xenconsoled.service xen-qemu-dom0-disk-backend.service
	/usr/bin/systemctl disable xenstored.service
	/usr/bin/systemctl disable xenconsoled.service
	/usr/bin/systemctl disable xen-qemu-dom0-disk-backend.service
fi
%else
if [ $1 = 0 ]; then
	/sbin/chkconfig --del xencommons
	/sbin/chkconfig --del xen-watchdog
fi
%endif

%if %with_systemd
%postun runtime
%systemd_postun
%endif

%post libs -p /sbin/ldconfig
%postun libs -p /sbin/ldconfig

%if %build_hyp
%post hypervisor
if [ $1 == 1 -a -f /sbin/grub2-mkconfig -a -f /boot/grub2/grub.cfg ]; then
  /sbin/grub2-mkconfig -o /boot/grub2/grub.cfg
fi

%postun hypervisor
if [ -f /sbin/grub2-mkconfig -a -f /boot/grub2/grub.cfg ]; then
  /sbin/grub2-mkconfig -o /boot/grub2/grub.cfg
fi
%endif

%if %{with_ocaml}
%post ocaml

%preun ocaml

%if %with_systemd_presets
%postun ocaml
%systemd_postun
%endif
%endif

%clean
rm -rf %{buildroot}

# Base package contains xl, xendomains & python stuff
%{!?buildsubdir: %global buildsubdir xen-%{version}}
%files
%defattr(-,root,root)

%{_bindir}/xencons
%if 0%{rhel} <= 7
%{python_sitearch}/xen
%{python_sitearch}/xen-*.egg-info
%endif
%if 0%{rhel} >= 8
%{python3_sitearch}/xen
%{python3_sitearch}/xen-*.egg-info
%endif


# Startup script
# Guest autostart links
%dir %{_sysconfdir}/xen/auto
# Autostart of guests
%config(noreplace) %{_sysconfdir}/sysconfig/xendomains

%if %with_systemd
%{_unitdir}/xendomains.service
%{_libexecdir}/xendomains
/usr/lib/modules-load.d/xen.conf
/usr/lib/tmpfiles.d/xen.conf
%else
/etc/rc.d/init.d/xendomains
%endif
/etc/rc.d/init.d/xendriverdomain

%files libs
%defattr(-,root,root)
%{_libdir}/*.so.*
%{_libdir}/xenfsimage

# All runtime stuff except for XenD/xm python stuff
%files runtime -f runtime.lst
%defattr(-,root,root)

# Hotplug rules
#%config(noreplace) %{_sysconfdir}/udev/rules.d/*

%dir %{_sysconfdir}/xen
%dir %{_sysconfdir}/xen/scripts/
%{_sysconfdir}/xen/scripts/*
%{_sysconfdir}/rc.d/init.d/xencommons
%{_sysconfdir}/rc.d/init.d/xen-watchdog
%{_sysconfdir}/bash_completion.d/xl.sh

# These files are in the main xen package
%exclude %{_sysconfdir}/rc.d/init.d/xendomains

## Package the systemd scripts...
%if %with_systemd
%exclude %{_unitdir}/xendomains.service
%{_unitdir}/*
%endif

%config(noreplace) %{_sysconfdir}/sysconfig/xen*
%config(noreplace) %{_sysconfdir}/xen/xl.conf
%config(noreplace) %{_sysconfdir}/xen/cpupool
%config(noreplace) %{_sysconfdir}/xen/xlexample*
/usr/lib/xen/bin/*
/usr/lib/xen/libexec/qemu-bridge-helper

# Rotate console log files
%config(noreplace) %{_sysconfdir}/logrotate.d/xen

# Programs run by other programs
%dir /usr/lib/xen
%if %with_systemd
%dir %{_libexecdir}
%{_libexecdir}/*
%endif

# QEMU runtime files
#%{_datadir}/xen/qemu/*
%{_datadir}/qemu-xen/qemu/*

# man pages
%{_mandir}/man1/xentop.1*
%{_mandir}/man1/xentrace_format.1*
%{_mandir}/man8/xentrace.8*
%{_mandir}/man1/xl.1*
%{_mandir}/man5/xl.cfg.5*
%{_mandir}/man5/xl.conf.5*
%{_mandir}/man5/xlcpupool.cfg.5*
%{_mandir}/man1/xenstore*

%if 0%{rhel} <= 7
%{python_sitearch}/xenfsimage.so
%{python_sitearch}/grub
%{python_sitearch}/pygrub-*.egg-info
%endif
%if 0%{rhel} >= 8
%{python3_sitearch}/xenfsimage*
%{python3_sitearch}/grub
%{python3_sitearch}/pygrub-*.egg-info
%endif
# pvgrub, hvmloader etc
%dir /usr/lib/xen/boot
/usr/lib/xen/boot/*

# General Xen state
%dir %{_localstatedir}/lib/xen
%dir %{_localstatedir}/lib/xen/dump
%dir %{_localstatedir}/lib/xen/images

# Xenstore persistent state
%dir %{_localstatedir}/lib/xenstored
# Xenstore runtime state
%ghost %attr(755,root,root) %{_localstatedir}/run/xenstored

%{_bindir}/xen-cpuid
%{_bindir}/xenstore
%{_bindir}/xenstore-*
%{_bindir}/pygrub
%{_bindir}/xenalyze
%{_bindir}/xentrace*

%{_bindir}/xen-detect
%{_bindir}/xencov_split

# Xen logfiles
%dir %{_localstatedir}/log/xen

# Guest/HV console logs
%dir %{_localstatedir}/log/xen/console

# exclude, already in `files xen`
%exclude %{_sysconfdir}/sysconfig/xendomains
%if %with_systemd
%exclude %{_libexecdir}/xendomains
%endif

%files hypervisor
%if %build_hyp
%defattr(-,root,root)
/boot/xen-*.gz
/boot/xen.gz
/boot/xen*.config
%if %build_xsm
%dir /boot/flask
/boot/flask/xenpolicy*
%endif
%endif

%files doc
%defattr(-,root,root)
%doc %{_datadir}/doc/xen/*
%{_mandir}/*

%files devel
%defattr(-,root,root)

%{_includedir}/*.h
%dir %{_includedir}/xen
%{_includedir}/xen/*
%dir %{_includedir}/xenstore-compat
%{_includedir}/xenstore-compat/*
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc

%files licenses
%defattr(-,root,root)
%doc licensedir/*

%if %{with_ocaml}
%files ocaml
%defattr(-,root,root)
%{ocamllib}/xen*
%exclude %{ocamllib}/xen*/*.a
%exclude %{ocamllib}/xen*/*.cmxa
%exclude %{ocamllib}/xen*/*.cmx
%{ocamllib}/stublibs/*.so
%{ocamllib}/stublibs/*.so.owner
%{_sbindir}/oxenstored
%config(noreplace) %{_sysconfdir}/xen/oxenstored.conf

%files ocaml-devel
%defattr(-,root,root)
%{ocamllib}/xen*/*.a
%{ocamllib}/xen*/*.cmxa
%{ocamllib}/xen*/*.cmx
%endif

%changelog
* Thu Nov 05 2020 - Steven Haigh <netwiz@crc.id.au> - 4.13.2-7
- Update to 4.13.2

* Tue Oct 27 2020 - Steven Haigh <netwiz@crc.id.au> - 4.13.1-6
- XSA-331 - Race condition in Linux event handler may crash dom0
- XSA-332 - Rogue guests can cause DoS of Dom0 via high frequency events
- XSA-345 - x86: Race condition in Xen mapping code
- XSA-346 - undue deferral of IOMMU TLB flushes
- XSA-347 - unsafe AMD IOMMU page table updates

* Tue Sep 29 2020 - Steven Haigh <netwiz@crc.id.au> - 4.13.1-5
- CVE-2020-25602 / XSA-333 - x86 pv: Crash when handling guest access to MSR_MISC_ENABLE
- CVE-2020-25598 / XSA-334 - Missing unlock in XENMEM_acquire_resource error path
- CVE-2020-25604 / XSA-336 - race when migrating timers between x86 HVM vCPU-s
- CVE-2020-25595 / XSA-337 - PCI passthrough code reading back hardware registers
- CVE-2020-25597 / XSA-338 - once valid event channels may not turn invalid
- CVE-2020-25596 / XSA-339 - x86 pv guest kernel DoS via SYSENTER
- CVE-2020-25603 / XSA-340 - Missing memory barriers when accessing/allocating an event channel
- CVE-2020-25600 / XSA-342 - out of bounds event channels available to 32-bit x86 domains
- CVE-2020-25599 / XSA-343 - races with evtchn_reset()
- CVE-2020-25601 / XSA-344 - lack of preemption in evtchn_reset() / evtchn_destroy()

* Wed Jul 08 2020 - Steven Haigh <netwiz@crc.id.au> - 4.13.1-4
- CVE-2020-15566 / XSA-317 - Incorrect error handling in event channel port allocation
- CVE-2020-15563 / XSA-319 - inverted code paths in x86 dirty VRAM tracking
- CVE-2020-15565 / XSA-321 - insufficient cache write-back under VT-d
- CVE-2020-15564 / XSA-327 - Missing alignment check in VCPUOP_register_vcpu_info
- CVE-2020-15567 / XSA-328 - non-atomic modification of live EPT PTE

* Wed May 27 2020 - Steven Haigh <netwiz@crc.id.au> - 4.13.1-3
- Update to 4.13.1

* Tue Apr 14 2020 - Steven Haigh <netwiz@crc.id.au> - 4.13.0-3
- CVE-2020-11739 / XSA-314 - Missing memory barriers in read-write unlock paths
- CVE-2020-11742 / XSA-318 - Bad continuation handling in GNTTABOP_copy
- CVE-2020-11740,CVE-2020-11741 / XSA-313 - multiple xenoprof issues
- CVE-2020-11743 / XSA-316 - Bad error path in GNTTABOP_map_grant

* Thu Mar 05 2020 - Steven Haigh <netwiz@crc.id.au> - 4.13.0-2
- Fix syntax error in xen-network-common.sh
- Add perl dependency

* Thu Dec 19 2019 - Steven Haigh <netwiz@crc.id.au> - 4.13.0-1
- Update to public release of Xen 4.13.0

* Wed Dec 4 2019 - Steven Haigh <netwiz@crc.id.au> - 4.13.0-0.1
- Initial Test Build.
